module hw1
import StdEnv


//  Given n compute the sum of 1 * 1 + 2 * 2 + ... + n * n.
//  Given n and k, k<n, compute the value of n choose k (n(n-1)...(n-k+1)) / (12...k).
//  It must be solved without using a factorial function. (???????????)

f1 :: Int -> Int

f1 n
|n == 0 = 0
= n * n + f1 (n - 1)

//Start = f1 5




fac :: Int -> Int
fac n 
|n == 0 = 1
= n * fac (n-1)

// Start = fac 5

nck :: Int Int -> Int
nck n k = fac n / (fac k * fac (n - k))

Start = nck 6 3




/*
comb :: Int Int -> Int
comb n k
|n == (n - k + 1) = 1
= n  * comb (n - 1) k
calck :: Int -> Int

calck k
|k== 0= 1
= k * calck (k - 1)

comb2 :: Int Int -> Int
comb2 n k 
|k == 0 = 1
= comb n k / calck k
//Start = comb  5 2
*/
