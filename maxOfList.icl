module maxOfList
import StdEnv


//- Recursive
mol :: [Int] -> Int
mol [x] = x
mol [x:xs]
| mol xs > x = mol xs
= x
//Start = mol [1..10]  //10

//- foldr
mof :: [Int] -> Int
mof a = foldr (\x y = if (x >= y) x  y) 0  a
//Start = mof [10,1,3,4,5,3,7,6,3,9,3,2,6,3]  //10

//- via sorting
mols :: [Int] -> Int
mols x = last (sort x)

//Start = mols [10,1,3,4,5,3,7,6,3,9,3,2,6,3]