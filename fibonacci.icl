fiba 1 _ b = b
fiba n a b = fiba (n - 1) b (a+b)


FibList n = [fiba x 0 1 \\ x <- [1..n]]
 
//Start = FibList 8 //[1,1,2,3,5,8,13,21]
//Start = FibList -3 //[]
//Start = FibList 0 //[]
