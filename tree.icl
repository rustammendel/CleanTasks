module tree
import StdEnv

:: Tree a = Node a (Tree a) (Tree a) | Leaf

//example tree

ourTree :: (Tree Int)
ourTree = Node 15(Node 3(Node 1 Leaf Leaf)(Node 10(Node 7 Leaf (Node 8 Leaf Leaf))(Node 13 (Node 11 Leaf Leaf) Leaf)))(Node 20 (Node 18 Leaf (Node 19 Leaf Leaf)) (Node 21 Leaf (Node 26 (Node 24 Leaf Leaf) (Node 28 Leaf Leaf))))

//Get every value and its depth 
getValNDepthList :: (Tree a) Int -> [(a,Int)]
getValNDepthList Leaf n = []
getValNDepthList (Node x l r) n = getValNDepthList l (n+1) ++ [(x ,n)] ++ getValNDepthList r (n+1)
//Start = getValNDepthList ourTree 0


//basics:
goL :: (Tree a) ->  (Tree a)
goL (Node _ l _) = l


goR :: (Tree a) ->  (Tree a)
goR (Node _ _ r) = r


extractNode :: (Tree a) -> a
extractNode Leaf = abort "u fucked!"
extractNode (Node x _ _ ) = x


isLeaf :: (Tree a)  -> Bool
isLeaf Leaf = True
isLeaf _ = False


////Two different versions of tracerouting (designed for netwoek tree ):
:: Router = { nodeName :: String, activeStatus :: Bool}
:: Network = Node Router Network Network | Termination

//Trace route from node n to root, This assumes both n and root exist in tree
traceroute :: Router Router (Network ) -> [Router] //|Eq Router
traceroute n root tree
|parent <> root = [parent] ++ traceroute parent root tree
=[root]
where 
	parent = findParent n tree // adapt this fuction  from this rep: https://github.com/ParadoxChains/BScFunctionalProgramming-2019-Fall/blob/master/Consultation_Notes/TreeReview.icl

//Start = traceroute r22 (extractNode bigNetwork) bigNetwork
 
//standalone route tracer. Between root node and given "node" argument in given tree
tr :: Router (Network) [Router] ->[Router]
tr node Termination list = list
tr node (Node x l r) list
|x == node = list ++ [node]
|isMember node (tr node l (list ++ [x])) = tr node l (list ++ [x])
|isMember node (tr node r (list ++ [x])) = tr node r (list ++ [x])
=list
