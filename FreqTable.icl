module FreqTable
import StdEnv

/*
    1.Check if a number is palindrom. (12321 - True, 1234 False)
    2.Define the function MakeFrequenceTable that makes a frequency table for the elements of a list.
The table consists of a list of lists. Each sub-list consists of an element from the original list, number of occurrences and its frequency (percentage).
Examples:
MakeFrequenceTable [] = [[]]
MakeFrequenceTable [1] = [[1,1,100]]
MakeFrequenceTable [1,2] = [[1,1,50],[2,1,50]]
MakeFrequenceTable [1..10] = [[1,1,10],[2,1,10],[3,1,10],[4,1,10],[5,1,10],[6,1,10],[7,1,10],[8,1,10],[9,1,10],[10,1,10]]
MakeFrequenceTable [1,3,2,3,2] = [[1,1,20],[3,2,40],[2,2,40]]
MakeFrequenceTable :: [Int] -> [[Int]] 

*/


	///Task 2: 
calc :: [Int] Int -> [[Int]]
calc [] i = []
calc x i = [[hd x] ++ [cnt] ++ [freq] ] ++ calc ( filter ( (<>) (hd x) ) (tl x) ) i
where 
	cnt = length( filter( (==) (hd x) ) x )
	freq = cnt * 100 / i

MakeFrequenceTable :: [Int] -> [[Int]]
MakeFrequenceTable [] = [] 
MakeFrequenceTable x = calc x (length x)

Start = MakeFrequenceTable [1,3,2,3,2] // [[1,1,20],[3,2,40],[2,2,40]]


	///Task 2 with List comp.
count n x = length( filter( (==) (n)) x)  //Counts occurence of n in x list

calcPerc n x = ((count n x) * 100 ) / length x //Calculates percentage of n in x

freq x = [[a] ++ [count a x ] ++ [calcPerc a x] \\ a <- n] //Generates list which consisted of element, its count and percentage.
where n = removeDup x

Start = freq [1,3,2,3,2]














