module dict
import StdEnv

//find item with biggest key in dictionary
bigDic :: [(Int,a)] -> (Int,a) 
bigDic [(key,value)] = (key,value)
bigDic [(key,value):xs]
| fst (bigDic xs) > key = bigDic xs
=(key, value)
