module mostOccuringChar
import StdEnv


strToChar :: String -> [Char] //~Converts String to list of Char
strToChar str = [ch \\ ch <-: str]

count :: Char [Char] -> Int //~Count how many times n occurs in x Char-list 
count n x = length( filter( (==) (n)) x)  

cntList :: [Char] -> [Int] //~Generates list of occurences of elements in a Char-list
cntList x = [count a x \\  a <- (removeDup x )] 

indexOvMax :: [Char] -> Int //~Finds index of most occuring Char from Char-array 
indexOvMax x = hd ([imax \\  cntOfElem <- (cntList  x) & imax <-[0..]  | cntOfElem == (maxList (cntList x))])

mostOccrChar :: String -> Char //~Finds Most Occuring Char in String 
mostOccrChar str = (removeDup (strToChar str))!!(indexOvMax (strToChar str)) 
 
Start  = mostOccrChar "helloworldteststring"
