module hw3
import StdEnv

//Task 1 :  Using foldr or foldl, write a function that will return true if every element in a list is even. 
	//For empty list, the function should return False. 

AllEven :: [Int] -> Bool
AllEven [] = False
AllEven x = foldr (&&) True ( map (\x = isEven x) x)

//Start = AllEven [2,3,5,3,5,2,1] //False
//Start = AllEven [1..4] //False
//Start = AllEven [2,4,6,8,10] //True
//Start = AllEven [] //False

// // //

//Task 2 : Given a list of two numbers n and k, generate a list of k pieces of multiples of n.
// For lists with only 1 member it should return that number back. For empty list it should return an empty list. 

mt n k = map (\k = k * n) [1..k]
//Start = mt 2 3
Multiples :: [Int] -> [Int]
Multiples x 
|length x == 1 = x
|length x == 0 = []

Multiples [n:k]= mt n (last k)

//Start = Multiples [2,3] //[2,4,6]
//Start = Multiples [4,8] //[4,8,12,16,20,24,28,32]
//Start = Multiples [1,1] //[1]
//Start = Multiples [4] //[4]
//Start = Multiples [] //[]

// // // 

//Task 3 :  Write a function that will calculate the average from the maximum elements of sublists. Empty sublists will be ignored.
// Empty case is required, when the output must be -1. */
 

malast :: [Int] -> Int 
malast [x] = x
malast [x:xs] = malast xs
malast [] = 0
//Start = malast []
sormap :: [[Int]] -> [Int]
sormap x = map malast (  map (\x = sort x ) x )
MaxAverages :: [[Int]] -> Int
MaxAverages [[]] = -1
MaxAverages x = avg ( sormap x ) 

//Start = MaxAverages [ [4,2,5] , [2,4,1] , [1,2,3], [1..8]]
//Start = MaxAverages [ [4,2,5] , [2,4,1] , [1,2,3], [1..8]] //5
//Start = MaxAverages [[]]   // -1
//Start = MaxAverages [ [1], [0], [1], [] ] // 0

// // //
