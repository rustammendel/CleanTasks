module isPrime
import StdEnv


isPrime_aux :: Int Int -> Bool
isPrime_aux n i 
|n <= 2 = n == 2          //check if n is 2 (2 is oddest prime :P )
|n rem i == 0 = False     //if divisible by m then not prime
|i * i > n = True         //finish computation if square of i is greater than n
=isPrime_aux n (i+1)      //to check if next number (i+1) is divisor of n


isPrime :: Int -> Bool
isPrime n = isPrime_aux n 2

//Start = isPrime 11

/*
isPrime :: Int -> Bool
isPrime n
| n <= 1 = False
= isEmpty[x\\x<-[2..(n-1)]|n rem x == 0]


isPrime :: Int -> Bool
isPrime x = and[x rem n <> 0\\n <- [2..(x-1)]]
*/
