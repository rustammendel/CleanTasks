module palindrome
import StdEnv

int2list :: Int -> [Int] //converts integer to list
int2list n
|n == 0 =[]
= [n rem 10 : int2list (n / 10)]

//Start = int2list 1234

palinka :: Int -> Bool
palinka n 
|int2list n == reverse (int2list n) =True  //checks if list equal to its reverse
=False

//Start = palinka 123432
///
